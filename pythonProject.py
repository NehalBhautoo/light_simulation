#import libraries

import opc
from nanpy import ArduinoApi
from nanpy import SerialManager
import time
import random
from random import randint

#connection to simulator
client = opc.Client('localhost:7890')

#serial connection via port 5(COM5)
connection = SerialManager('COM5')    
a = ArduinoApi(connection=connection)

#sound sensor connected in pin 8 of Arduino
soundSensor = 8
a.pinMode(soundSensor, a.INPUT)

# assigning variables
ldr = 14
fps = 60

#Infinite while loop
while True:


    try:

    	#getting user input
        num = int(input("Choose :\n" +
                        "1 to generate random colors\n" +
                        "2 to switch between red, green, blue\n" +
                        "3 to fade led\n" +
                        "4 to use sound Sensor\n" +
                        "5 to use LDR\n" +
                        "Press CNTLR + C to change/EXIT animation\n"))

    #Input validation   
    except ValueError:

    	#Ignore below codes for invalid input
        print("Please enter a number\n")
        continue
    
    if(num < 0):
        
        print("Sorry, response must not be negative\n")
        continue
    
    elif(num >= 6):

        print("Enter a value less than 4\n")
        continue

    #First animation
    elif(num == 1):

    	#assigning 360 led_color to black
        led_color=[(0,0,0)]*360
        
        try:
            
            while True:

            	#Display 360 random colours
                for x in range(0,360):
                	
                    #set random value red,green,blue between 1 and 255
                    red = random.randint(1,255)
                    green = random.randint(1,255)
                    blue = random.randint(1,255)

                    #assign each led random colours
                    led_color[x]=(red,green,blue)
                    time.sleep(1 / fps) # time delay(1/60) 
                    client.put_pixels(led_color) #Display on simulator
        
        #press CTRL+C, to exit/cancel animation 
        except KeyboardInterrupt:
            
            print ("Cancel..")
            #reset led animation to black
            led_color = [(0,0,0)]*360
            client.put_pixels(led_color)
            continue;

    #second animation
    if(num == 2):

        n_pixels = 360  
        
        try:
            
            while True:
                        
                for c in range(4):
     				
     		    #assign a pixels list with no values
                    pixels = []

                    #assign a pixels list with values 0
                    rgb = [0, 0, 0]
                            
                    if c < 3:

                    	#rgb with modulo remainder 1
                        rgb[c%3] = 255
                        rgb = tuple(rgb)
                        
                        #i in range of 360    
                        for i in range(n_pixels):

                            #adding rgb in variable pixels
                            pixels.append(rgb)
                            client.put_pixels(pixels)
                            time.sleep(1 / fps)

        except KeyboardInterrupt:

            print ("Cancel..")
            led_color = [(0,0,0)]*360
            client.put_pixels(led_color)
            continue

    elif(num == 3):

        try:

            while True :
                
                for j in range (0,3) :
                    
                    for k in range (0,256) :
                        
                            led_colour = [(k,0,0)]*360
                            client.put_pixels(led_colour)
                            time.sleep(0.01)
                    for k in reversed(range(0,256)) :
                            led_colour = [(k,0,0)]*360
                            client.put_pixels(led_colour)
                            time.sleep(0.01)
                    for k in range (0,256) :
                            led_colour = [(0,k,0)]*360
                            client.put_pixels(led_colour)
                            time.sleep(0.01)
                    for k in reversed(range(0,256)) :
                            led_colour = [(0,k,0)]*360
                            client.put_pixels(led_colour)
                            time.sleep(0.01)
                    for k in range (0,256) :
                            led_colour = [(0,0,k)]*360
                            client.put_pixels(led_colour)
                            time.sleep(0.01)
                    for k in reversed(range(0,256)) :
                            led_colour = [(0,0,k)]*360
                            client.put_pixels(led_colour)
                            time.sleep(0.01)

                
        except KeyboardInterrupt:

            print ("Cancel..")
            led_color = [(0,0,0)]*360
            client.put_pixels(led_color)
            continue

    #Third animation
    elif(num == 4):

        try:

            while True:

               #reading values from sound sensor
               SensorData = a.digitalRead(soundSensor)
               print(SensorData)

               led_color2=[(255,255,255)]*360
               led_color3=[(0,0,0)]*360
               
               #condition for intensity of soundData
               if(SensorData == 0):

                client.put_pixels(led_color2)

               elif(SensorData == 1):

                client.put_pixels(led_color3)

        except KeyboardInterrupt:

            print ("Cancel..")
            led_color = [(0,0,0)]*360
            client.put_pixels(led_color)
            continue

    elif(num == 5):

        try:

            while 1:
            	
            	#reading values of r from LDR
                r = a.analogRead(ldr)
                print(r)
               
                if(r == 0):

                    led_color2=[(0,0,0)]*360
                    client.put_pixels(led_color2)

                elif(r <= 10):

                    led_color3=[(0,0,255)]*360
                    client.put_pixels(led_color3)

                elif(r <= 20):

                    led_color4=[(0,255,255)]*360
                    client.put_pixels(led_color4)

                elif(r <= 30):

                    led_color5=[(255,0,255)]*360
                    client.put_pixels(led_color5)

                elif(r <= 40):

                    led_color6=[(255,255,0)]*360
                    client.put_pixels(led_color6)

                elif(r <= 50):

                    led_color7=[(0,255,0)]*360
                    client.put_pixels(led_color7)

                elif(r <= 200):

                    led_color8=[(164,203,156)]*360
                    client.put_pixels(led_color8)
                    
                elif(r <= 400):

                    led_color8=[(24,184,213)]*360
                    client.put_pixels(led_color8)

                else:

                    led_color7=[(255,255,255)]*360
                    client.put_pixels(led_color7)
                    
        except KeyboardInterrupt:

                print ("Cancel..")
                led_color = [(0,0,0)]*360
                client.put_pixels(led_color)
                continue
            

        

































        
